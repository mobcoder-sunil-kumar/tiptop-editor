import './App.css';
import Tiptop from './pages/Tiptop';

function App() {
  return (
    <div className="App">
      <Tiptop />
    </div>
  );
}

export default App;

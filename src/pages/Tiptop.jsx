import React from 'react';
import { EditorContent, Editor } from '@tiptap/react';
import StarterKit from '@tiptap/starter-kit';


import '../styles.css';
import TiptopMenu from '../components/TipTopMenu';

const Tiptop = () => {
  const editor = new Editor({
    extensions: [StarterKit],
    content: content,
    onUpdate: ({ editor }) => {
      const data = editor.getJSON();
      localStorage.setItem("data", JSON.stringify(data))
      console.log(data, 'data on change');
    },
  });

  return (
    <div style={{
      padding: '10px',
      border: '5px solid red'
    }}>
      <TiptopMenu editor={editor} />
      <EditorContent
      style={{
        padding: '5px',
        border: '5px solid black',
        textAlign: 'left'
      }}
       editor={editor} />
    </div>
  );
};

export default Tiptop;

const content1 = ``
const content = `
<h2>
  Hi there,
</h2>
<p>
  this is a <em>basic</em> example of <strong>tiptap</strong>. Sure, there are all kind of basic text styles you’d probably expect from a text editor. But wait until you see the lists:
</p>
<ul>
  <li>
    That’s a bullet list with one …
  </li>
  <li>
    … or two list items.
  </li>
</ul>
<p>
  Isn’t that great? And all of that is editable. But wait, there’s more. Let’s try a code block:
</p>
<pre><code class="language-css">body {
display: none;
}</code></pre>
<p>
  I know, I know, this is impressive. It’s only the tip of the iceberg though. Give it a try and click a little bit around. Don’t forget to check the other examples too.
</p>
<blockquote>
  Wow, that’s amazing. Good work, boy! 👏
  <br />
  — Mom
</blockquote>
`;
